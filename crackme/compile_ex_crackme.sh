#!/bin/bash

gcc -static ex_crackme.c -o ex_crackme; strip ex_crackme
upx -9 -o ex_crackme_upx ex_crackme
rm -f ex_crackme
mv ex_crackme_upx ex_crackme
