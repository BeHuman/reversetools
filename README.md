# Reverse Tools
Reverse Engineering Toolbox.

Boite à outils pour Reverse Engineering.

Programmes Utilisés  
---------------------
GDB                  
GDB64                
OBJDump              
Radare2              
Dot                  
NM                   
Strings              
LTrace               
STrace               

DISTRIBUTION TEST
=================
Debian 8 Jessie 64bit

Ubuntu 16.04 Xenial Xerus 64bit

Ubuntu 16.10 Yakkety Yak 64bit

INSTALLATION
============
make install

UNINSTALLATION
==============
make uninstall

DEPENDENCES
===========
make deps

OPTIONAL DEPENDENCES
====================
make odeps

EXECUTE WITHOUT INSTALLATION
============================
./reversetools.sh

DEPENDENCES DEBIAN JESSIE
=========================
sudo apt-get install python-gi gir1.2-gtksource-3.0 gir1.2-vte-2.91 gir1.2-webkit-3.0 gir1.2-javascriptcoregtk-3.0 radare2 gdb graphviz

Dependences Listing     
------------------------
python-gi               
gir1.2-gtksource-3.0    
gir1.2-vte-2.91         
gir1.2-webkit-3.0       
radare2                 
gdb                     
graphviz                

OPTIONAL DEPENDENCES
====================
sudo apt-get install ltrace strace

Optional Dependences Listing  
------------------------------
ltrace                        
strace                        

FIX ERRORS WEBKIT DEPENDENCES
=====================
```
Traceback (most recent call last):
  File "/usr/share/reversetools/glade2script.py", line 4515, in <module>
    from gi.repository import WebKit as webkit
ImportError: cannot import name WebKit
```
sudo apt-get install gir1.2-webkit-3.0

FIX ERRORS TERMINAL DEPENDENCES
=====================
```
Traceback (most recent call last):
  File "/usr/share/reversetools/glade2script.py", line 4585, in <module>
    m = Gui()
  File "/usr/share/reversetools/glade2script.py", line 1748, in __init__
    self.make_terminal()
  File "/usr/share/reversetools/glade2script.py", line 1946, in make_terminal
    self.terminal.spawn_sync(
AttributeError: 'Terminal' object has no attribute 'spawn_sync'
```
sudo apt-get install gir1.2-vte-2.91


SCREENSHOT
==========
![Capture](https://git.framasoft.org/BeHuman/reversetools/raw/88d447da0bfbc98ef2e28118a8d81ef3db68b79b/screen.png)
![Capture](https://git.framasoft.org/BeHuman/reversetools/raw/88d447da0bfbc98ef2e28118a8d81ef3db68b79b/screen2.png)
![Capture](https://git.framasoft.org/BeHuman/reversetools/raw/88d447da0bfbc98ef2e28118a8d81ef3db68b79b/screen3.png)
