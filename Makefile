all : install
install :
	sudo apt-get install python-gi gir1.2-gtksource-3.0 gir1.2-vte-2.91 gir1.2-webkit-3.0 radare2 gdb graphviz ltrace strace
	sudo mkdir /usr/share/reversetools
	sudo cp reversetools.sh /usr/share/reversetools/reversetools.sh
	sudo mkdir /usr/share/reversetools/includes
	sudo mkdir /usr/share/reversetools/includes/language-specs
	sudo cp includes/init.inc.sh /usr/share/reversetools/includes/init.inc.sh
	sudo cp includes/prefs.inc.sh /usr/share/reversetools/includes/prefs.inc.sh
	sudo cp includes/gdb.inc.sh /usr/share/reversetools/includes/gdb.inc.sh
	sudo cp includes/trace.inc.sh /usr/share/reversetools/includes/trace.inc.sh
	sudo cp includes/search.inc.sh /usr/share/reversetools/includes/search.inc.sh
	sudo cp includes/breakpoints.inc.sh /usr/share/reversetools/includes/breakpoints.inc.sh
	sudo cp includes/checkpoints.inc.sh /usr/share/reversetools/includes/checkpoints.inc.sh
	sudo cp includes/asm2c.inc.sh /usr/share/reversetools/includes/asm2c.inc.sh
	sudo cp includes/graphic.inc.sh /usr/share/reversetools/includes/graphic.inc.sh
	sudo cp includes/webui.inc.sh /usr/share/reversetools/includes/webui.inc.sh
	sudo cp includes/language-specs/asm-att.lang /usr/share/reversetools/includes/language-specs/asm-att.lang
	sudo cp includes/language-specs/asm-intel.lang /usr/share/reversetools/includes/language-specs/asm-intel.lang
	sudo cp reversetools /usr/bin/reversetools
	sudo cp reversetools.glade /usr/share/reversetools/reversetools.glade
	sudo cp glade2script.py /usr/share/reversetools/glade2script.py
	sudo cp applications/reversetools.desktop /usr/share/applications/reversetools.desktop
	sudo chmod 755 /usr/share/reversetools/reversetools.sh
	sudo chmod 755 /usr/bin/reversetools
	sudo chmod 755 /usr/share/reversetools/glade2script.py
	sudo chmod 755 /usr/share/applications/reversetools.desktop
deps :
	sudo apt-get install python-gi gir1.2-gtksource-3.0 gir1.2-vte-2.91 gir1.2-webkit-3.0 radare2 gdb graphviz 
odeps :
	sudo apt-get install ltrace strace
uninstall :
	if [ -f /usr/share/reversetools/reversetools.sh ]; then sudo rm -f /usr/share/reversetools/reversetools.sh; fi
	if [ -f /usr/share/reversetools/includes/init.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/init.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/prefs.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/prefs.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/gdb.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/gdb.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/trace.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/trace.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/search.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/search.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/breakpoints.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/breakpoints.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/checkpoints.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/checkpoints.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/asm2c.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/asm2c.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/graphic.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/graphic.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/webui.inc.sh ]; then sudo rm -f /usr/share/reversetools/includes/webui.inc.sh; fi
	if [ -f /usr/share/reversetools/includes/language-specs/asm-att.lang ]; then sudo rm -f /usr/share/reversetools/includes/language-specs/asm-att.lang; fi
	if [ -f /usr/share/reversetools/includes/language-specs/asm-intel.lang ]; then sudo rm -f /usr/share/reversetools/includes/language-specs/asm-intel.lang; fi
	if [ -f /usr/bin/reversetools ]; then sudo rm -f /usr/bin/reversetools; fi
	if [ -f /usr/share/reversetools/reversetools.glade ]; then sudo rm -f /usr/share/reversetools/reversetools.glade; fi
	if [ -f /usr/share/reversetools/glade2script.py ]; then sudo rm -f /usr/share/reversetools/glade2script.py; fi
	if [ -f /usr/share/reversetools/applications/reversetools.desktop ]; then sudo rm -f /usr/share/reversetools/applications/reversetools.desktop; fi
	if [ -d /usr/share/reversetools/includes/language-specs ]; then sudo rmdir /usr/share/reversetools/includes/language-specs; fi
	if [ -d /usr/share/reversetools/includes ]; then sudo rmdir /usr/share/reversetools/includes; fi
	if [ -d /usr/share/reversetools ]; then sudo rmdir /usr/share/reversetools; fi
