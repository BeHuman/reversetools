#!/bin/bash

##############################################################################################
if [[ $1 != 1 ]]; then
    "$PWD/glade2script.py" -g "$PWD/reversetools.glade" -d -s "$0 1" --terminal='_terminal:590x400' --sourceview="_sourceview,_boxgdb" --sourceview="_sourceview2,_boxobj" --sourceview="_sourceview3,_boxradare2" --sourceview="_sourceview4,_boxpseudocode"  --sourceview="_viewfunctions,_scrollfunctions" --sourceview="_viewstrings,_scrollstrings" --sourceview="_viewregisters,_scrollregisters" -t "@@_tgdbfunctions@@Name%%editable|Address%%editable" -t "@@_tbreakpoints@@%%editable"  -t "@@_tcheckpoints@@%%editable" --webkit='_graph,_boxgraph' --webkit='_webui,_boxwebui'
    exit
fi
PID=$$
FIFO=/tmp/FIFO${PID}
mkfifo $FIFO
###############################################################################################
rm -rf $HOME/.retdump/*
. includes/init.inc.sh
. includes/prefs.inc.sh
. includes/gdb.inc.sh
. includes/trace.inc.sh
. includes/search.inc.sh
. includes/breakpoints.inc.sh
. includes/checkpoints.inc.sh
. includes/graphic.inc.sh
. includes/webui.inc.sh
##########################################################################################
while read ligne; do
    if [[ "$ligne" =~ GET@ ]]; then
      eval ${ligne#*@}
      echo "DEBUG => in boucle bash :" ${ligne#*@}
    else
      echo "DEBUG=> in bash NOT GET" $ligne
      $ligne
   fi 
done < <(while true; do
    read entree < $FIFO
    [[ "$entree" == "QuitNow" ]] && break
    echo $entree   
done)
exit
