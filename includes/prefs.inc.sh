if [[ -f $HOME/.retdump/temporisation ]]; then
    TEMPORISATION=$(cat $HOME/.retdump/temporisation)
    echo "SET@_preftemporisation.set_text(\"$TEMPORISATION\")"
else
    TEMPORISATION=0
fi
function _preftemporisation() {
    TEMPORISATION="$(cut -d '@' -f1 --complement <<< $@ )"
    echo -n "$TEMPORISATION" > $HOME/.retdump/temporisation
}

#function _preftemporisation() {
#    TEMPORISATION="$(cut -d '@' -f1 --complement <<< $@ )"
#    echo -n "$TEMPORISATION" > $HOME/.retdump/temporisation
#}

if [[ -f $HOME/.retdump/temporun ]]; then
    TEMPORUN=$(cat $HOME/.retdump/temporun)
    echo "SET@_preftemporun.set_active($TEMPORUN)"
else
    TEMPORUN='False'
    echo "SET@_preftemporun.set_active($TEMPORUN)"
fi

function _preftemporun() {
    TEMPORUN="$(cut -d '@' -f1 --complement <<< $@ )"
    echo -n "$TEMPORUN" > $HOME/.retdump/temporun
}

if [[ -f $HOME/.retdump/tempocontinue ]]; then
    TEMPOCONTINUE=$(cat $HOME/.retdump/tempocontinue)
    echo "SET@_preftempocontinue.set_active($TEMPOCONTINUE)"
else
    TEMPOCONTINUE='False'
    echo "SET@_preftempocontinue.set_active($TEMPOCONTINUE)"
fi

function _preftempocontinue() {
    TEMPOCONTINUE="$(cut -d '@' -f1 --complement <<< $@ )"
    echo -n "$TEMPOCONTINUE" > $HOME/.retdump/tempocontinue
}
