function _refreshcheckpoints() {
    sleep 0.5
    echo "TERM@@SEND@@set logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo checkpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tcheckpoints"
    cat $HOME/.retdump/gdb.txt | while read checky; do
        bb=`echo "$checky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tcheckpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(5)'
}

function _deletecheckpoints() {
    sleep 0.5
    echo "TERM@@SEND@@delete checkpoints $CNCURRENT\nset logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo checkpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tcheckpoints"
    cat $HOME/.retdump/gdb.txt | while read checky; do
        bb=`echo "$checky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tcheckpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(5)'
}


function _addcheckpoints() {
    sleep 0.5
    echo "TERM@@SEND@@checkpoint *$CAENTRYCURRENT\nset logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo checkpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tcheckpoints"
    cat $HOME/.retdump/gdb.txt | while read checky; do
        bb=`echo "$checky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tcheckpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(5)'
}

CAENTRYCURRENT=""
function _entryaddcheckpoints() {
    CAENTRYCURRENT=$(cut -d '@' -f1 <<< $1)
    echo "SET@entrycheck=\"$CAENTRYCURRENT\""
}

CNCURRENT=0
CACURRENT='0x00000'
function _tcheckpoints() {
    arg1="$(cut -d '@' -f1 --complement <<< $@ )"
#    addr="$(cut -d 'x' -f1 --complement <<< $@)"
    CNCURRENT="$(echo "$arg1" | cut -d ' ' -f1)"
    CACURRENT="$(echo "$arg1" | cut -d ' ' -f5)"
}
