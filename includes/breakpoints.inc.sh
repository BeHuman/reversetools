function _refreshbreakpoints() {
    sleep 0.5
    echo "TERM@@SEND@@set logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo breakpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tbreakpoints"
    cat $HOME/.retdump/gdb.txt | while read breaky; do
        bb=`echo "$breaky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tbreakpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(4)'
}

function _deletebreakpoints() {
    sleep 0.5
    echo "TERM@@SEND@@delete breakpoints $BNCURRENT\nset logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo breakpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tbreakpoints"
    cat $HOME/.retdump/gdb.txt | while read breaky; do
        bb=`echo "$breaky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tbreakpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(4)'
}

function _disablebreakpoints() {
    sleep 0.5
    echo "TERM@@SEND@@disable breakpoints $BNCURRENT\nset logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo breakpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tbreakpoints"
    cat $HOME/.retdump/gdb.txt | while read breaky; do
        bb=`echo "$breaky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tbreakpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(4)'
}

function _enablebreakpoints() {
    sleep 0.5
    echo "TERM@@SEND@@enable breakpoints $BNCURRENT\nset logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo breakpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tbreakpoints"
    cat $HOME/.retdump/gdb.txt | while read breaky; do
        bb=`echo "$breaky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tbreakpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(4)'
}

function _addbreakpoints() {
    sleep 0.5
    out=`echo "$BAENTRYCURRENT" | grep -e '^[0-9][0-9].*'`
    if [[ $? -eq 0 ]]; then
        echo "TERM@@SEND@@break *0x0$BAENTRYCURRENT\nset logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo breakpoints\n"
    else
        echo "TERM@@SEND@@break *$BAENTRYCURRENT\nset logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo breakpoints\n"
    fi
    sleep 0.5
    echo "TREE@@CLEAR@@_tbreakpoints"
    cat $HOME/.retdump/gdb.txt | while read breaky; do
        bb=`echo "$breaky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tbreakpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
#    echo 'SET@_notebook1.set_current_page(4)'
}

BAENTRYCURRENT=""
function _entryaddbreakpoints() {
    BAENTRYCURRENT=$(cut -d '@' -f1 <<< $1)
    echo "SET@entrybreak=\"$BAENTRYCURRENT\""
}

BNCURRENT=0
BACURRENT='0x00000'
function _tbreakpoints() {
    arg1="$(cut -d '@' -f1 --complement <<< $@ )"
#    addr="$(cut -d 'x' -f1 --complement <<< $@)"
    BNCURRENT="$(echo "$arg1" | cut -d ' ' -f1)"
    BACURRENT="$(echo "$arg1" | cut -d ' ' -f5)"
}

