echo "TEXT@@SOURCE@@LANG@@_sourceview@@asm-att
TEXT@@SOURCE@@LANG@@_sourceview2@@asm-intel
TEXT@@SOURCE@@LANG@@_sourceview3@@asm-intel
TEXT@@SOURCE@@LANG@@_sourceview4@@c
TEXT@@SOURCE@@LANG@@_viewfunctions@@makefile
TEXT@@SOURCE@@LANG@@_viewstrings@@text
TEXT@@SOURCE@@LANG@@_viewregisters@@makefile"

echo "SET@_sourceview.set_wrap_mode(Gtk.WrapMode.CHAR)
SET@_sourceview.set_show_line_numbers(True)
SET@_sourceview.set_insert_spaces_instead_of_tabs(True)
SET@_sourceview.set_auto_indent(True)
SET@_sourceview.set_show_line_marks(True)
SET@_sourceview.set_draw_spaces(GtkSource.DrawSpacesFlags.ALL)
SET@_sourceview.set_highlight_current_line(True)
SET@_sourceview.set_indent_on_tab(True)
SET@_sourceview.set_tab_width(4)
SET@_sourceview.modify_font(Pango.FontDescription('monospace 9'))
TEXT@@SOURCE@@STYLE@@_sourceview@@oblivion"

echo "SET@_sourceview2.set_wrap_mode(Gtk.WrapMode.CHAR)
SET@_sourceview2.set_show_line_numbers(True)
SET@_sourceview2.set_insert_spaces_instead_of_tabs(True)
SET@_sourceview2.set_auto_indent(True)
SET@_sourceview2.set_show_line_marks(True)
SET@_sourceview2.set_draw_spaces(GtkSource.DrawSpacesFlags.ALL)
SET@_sourceview2.set_highlight_current_line(True)
SET@_sourceview2.set_indent_on_tab(True)
SET@_sourceview2.set_tab_width(4)
SET@_sourceview2.modify_font(Pango.FontDescription('monospace 9'))
TEXT@@SOURCE@@STYLE@@_sourceview2@@oblivion"

echo "SET@_sourceview3.set_wrap_mode(Gtk.WrapMode.CHAR)
SET@_sourceview3.set_show_line_numbers(True)
SET@_sourceview3.set_insert_spaces_instead_of_tabs(True)
SET@_sourceview3.set_auto_indent(True)
SET@_sourceview3.set_show_line_marks(True)
SET@_sourceview3.set_draw_spaces(GtkSource.DrawSpacesFlags.ALL)
SET@_sourceview3.set_highlight_current_line(True)
SET@_sourceview3.set_indent_on_tab(True)
SET@_sourceview3.set_tab_width(4)
SET@_sourceview3.modify_font(Pango.FontDescription('monospace 9'))
TEXT@@SOURCE@@STYLE@@_sourceview3@@oblivion"

echo "SET@_sourceview4.set_wrap_mode(Gtk.WrapMode.CHAR)
SET@_sourceview4.set_show_line_numbers(True)
SET@_sourceview4.set_insert_spaces_instead_of_tabs(True)
SET@_sourceview4.set_auto_indent(True)
SET@_sourceview4.set_show_line_marks(True)
SET@_sourceview4.set_draw_spaces(GtkSource.DrawSpacesFlags.ALL)
SET@_sourceview4.set_highlight_current_line(True)
SET@_sourceview4.set_indent_on_tab(True)
SET@_sourceview4.set_tab_width(4)
SET@_sourceview4.modify_font(Pango.FontDescription('monospace 9'))"

echo "SET@_viewfunctions.set_wrap_mode(Gtk.WrapMode.CHAR)
SET@_viewfunctions.set_show_line_numbers(True)
SET@_viewfunctions.set_insert_spaces_instead_of_tabs(True)
SET@_viewfunctions.set_auto_indent(True)
SET@_viewfunctions.set_show_line_marks(True)
SET@_viewfunctions.set_draw_spaces(GtkSource.DrawSpacesFlags.ALL)
SET@_viewfunctions.set_highlight_current_line(True)
SET@_viewfunctions.set_indent_on_tab(True)
SET@_viewfunctions.set_tab_width(4)
SET@_viewfunctions.modify_font(Pango.FontDescription('monospace 9'))"

echo "SET@_viewstrings.set_wrap_mode(Gtk.WrapMode.CHAR)
SET@_viewstrings.set_show_line_numbers(True)
SET@_viewstrings.set_insert_spaces_instead_of_tabs(True)
SET@_viewstrings.set_auto_indent(True)
SET@_viewstrings.set_show_line_marks(True)
SET@_viewstrings.set_draw_spaces(GtkSource.DrawSpacesFlags.ALL)
SET@_viewstrings.set_highlight_current_line(True)
SET@_viewstrings.set_indent_on_tab(True)
SET@_viewstrings.set_tab_width(4)
SET@_viewstrings.modify_font(Pango.FontDescription('monospace 9'))"

echo "SET@_viewregisters.set_wrap_mode(Gtk.WrapMode.CHAR)
SET@_viewregisters.set_show_line_numbers(True)
SET@_viewregisters.set_insert_spaces_instead_of_tabs(True)
SET@_viewregisters.set_auto_indent(True)
SET@_viewregisters.set_show_line_marks(True)
SET@_viewregisters.set_draw_spaces(GtkSource.DrawSpacesFlags.ALL)
SET@_viewregisters.set_highlight_current_line(True)
SET@_viewregisters.set_indent_on_tab(True)
SET@_viewregisters.set_tab_width(4)
SET@_viewregisters.modify_font(Pango.FontDescription('monospace 9'))"


echo "SET@window.maximize()
ITER@@_panedPosition"

echo "SET@_spinner.set_visible(False)"

. includes/asm2c.inc.sh

r2pm init
r2pm update
r2pm -i www-enyo

gdb_bin='gdb'
bin=""
function _panedPosition() {
    sleep 1
    echo "SET@_paned.set_position(750)"
}
function _binary() {
    rm -rf $HOME/.retdump/*
    bin=$(cut -d '@' -f1 <<< $1)
    echo "SET@_spinner.start()"
    echo "SET@_spinner.set_visible(True)"
    if [[ ! -d $HOME/.retdump ]]; then
        mkdir $HOME/.retdump
    fi
    echo -n '' > $HOME/.retdump/asm
    echo -n '' > $HOME/.retdump/asm2
    echo -n '' > $HOME/.retdump/asm3
    echo -n '' > $HOME/.retdump/functions
    
#    test_64bit=`file $bin | grep 'ELF 64-bit'`
#    if [[ $? -eq 0 ]]; then
#        gdb_bin='gdb64'
#    fi
    
    START=`objdump -f crackme/ex_crackme2 | tail -n -2 | head -n -1 | awk '{print $NF}'`
    
    $gdb_bin "$bin" -ex "disassemble $START" --batch > $HOME/.retdump/asm
    cp $HOME/.retdump/asm $HOME/.retdump/asm.origin
    echo "TEXT@@LOAD@@_sourceview@@$HOME/.retdump/asm
TEXT@@FIND@@_sourceview@@function main:@@scroll,select"

    radare2 "$bin" -c "pdf >> $HOME/.retdump/asm3" -q
    cp $HOME/.retdump/asm3 $HOME/.retdump/asm3.origin
    echo "TEXT@@LOAD@@_sourceview3@@$HOME/.retdump/asm3
TEXT@@FIND@@_sourceview3@@;-- sym.main:@@scroll,select"

    objdump --no-show-raw-insn -Mintel -D "$bin" > $HOME/.retdump/asm2
    cp $HOME/.retdump/asm2 $HOME/.retdump/asm2.origin
    echo "TEXT@@LOAD@@_sourceview2@@$HOME/.retdump/asm2
TEXT@@FIND@@_sourceview2@@<main>:@@scroll,select"


    echo "TREE@@CLEAR@@_tgdbfunctions"
    echo "TREE@@END@@_tgdbfunctions@@Start Point|$START"
    nm "$bin" | while read dis; do
        ddis=`echo $dis| cut -d' ' -f3`
        nnis=`echo $dis| cut -d' ' -f2`
        add=`echo $dis| cut -d' ' -f1`
        if [[ "$nnis" == "T" ]]; then
            if [[ "$ddis" != "" ]]; then
                #$gdb_bin "$bin" -ex "disassemble $ddis" --batch >> $HOME/.retdump/asm
                echo "TREE@@END@@_tgdbfunctions@@$ddis|$add"
                #echo "$dis" | cut -d' ' -f3 >> $HOME/.retdump/functions
                #radare2 "$bin" -c "pd@`echo $dis| cut -d' ' -f3` >> $HOME/.retdump/asm3" -q
            fi
        fi
    done
    echo 'SET@_notebook1.set_current_page(1)'
    echo "TREE@@FIND@@_tgdbfunctions@@@@main"
    
    out=`file $HOME/.retdump/asm | grep 'empty'`
    if [[ $? == 0 ]]; then
        echo 'SET@_notebook2.set_current_page(1)'
    fi
    out=`file $HOME/.retdump/asm2 | grep 'empty'`
    if [[ $? == 0 ]]; then
        echo 'SET@_notebook2.set_current_page(2)'
    fi
#    echo "TEXT@@LOAD@@_sourceview@@$HOME/.retdump/asm
#TEXT@@FIND@@_sourceview@@function main:@@scroll,select
#TEXT@@LOAD@@_viewfunctions@@$HOME/.retdump/functions"
#    
##    radare2 "$bin" -c "pdf >> $HOME/.retdump/asm3" -q
#    echo "TEXT@@LOAD@@_sourceview3@@$HOME/.retdump/asm3
#TEXT@@FIND@@_sourceview3@@;-- sym.main:@@scroll,select"
    
#    nm "$bin" | cut -d' ' -f3 | while read dis; do
#        if [[ "$dis" != "" ]]; then
#            $gdb_bin "$bin" -ex "disassemble $dis" --batch >> $HOME/.retdump/asm
#            echo $dis >> $HOME/.retdump/functions
#            radare2 "$bin" -c "pd@$dis >> $HOME/.retdump/asm3" -q
#        fi
#    done
#    nm "$bin" | cut -d' ' -f4 | while read dis; do
#        if [[ "$dis" != "" ]]; then
#            $gdb_bin "$bin" -ex "disassemble $dis" --batch >> $HOME/.retdump/asm
#            echo $dis >> $HOME/.retdump/functions
#            radare2 "$bin" -c "pd@$dis >> $HOME/.retdump/asm3" -q
#        fi
#    done
#    nm "$bin" | cut -d' ' -f19 | while read dis; do
#        if [[ "$dis" != "" ]]; then
#            $gdb_bin "$bin" -ex "disassemble $dis" --batch >> $HOME/.retdump/asm
#            echo $dis >> $HOME/.retdump/functions
#            radare2 "$bin" -c "pd@$dis >> $HOME/.retdump/asm3" -q
#        fi
#    done
##    nm -Dns "$bin" > $HOME/.retdump/functions


    
    
#    if [[ `cat $HOME/.retdump/asm3` == '' ]]; then
#        $gdb_bin "$bin" -ex "info functions" --batch | cut -d' ' -f3 | cut -d'@' -f1 | while read dis; do
#            radare2 "$bin" -c "pd@$dis >> $HOME/.retdump/asm3" -q
#        done
#    fi
#    nm "$bin" > $HOME/.retdump/functions
    
    
    strings "$bin" > $HOME/.retdump/strings
    echo "TEXT@@LOAD@@_viewstrings@@$HOME/.retdump/strings"
    
    
#    if [[ ! -f "$HOME/.retdump/radare2.cmd" ]]; then
#        echo "$START" > $HOME/.retdump/radare2.cmd
#        echo "af" >> $HOME/.retdump/radare2.cmd
#        echo "ag > $HOME/.retdump/dump.dot" >> $HOME/.retdump/radare2.cmd
#        
#        rm -rf $HOME/.retdump/*.svg
#        rm -rf $HOME/.retdump/*.dot
#        echo "0x00000000" > $HOME/.retdump/radare2_link.cmd
#        echo "af" >> $HOME/.retdump/radare2_link.cmd
#        echo "ag > $HOME/.retdump/0x00000000.dot" >> $HOME/.retdump/radare2_link.cmd
##    fi
#    radare2 "$bin" -i $HOME/.retdump/radare2.cmd -q
#    dot -Tsvg -Ln20 -LC2 -o$HOME/.retdump/dump.svg $HOME/.retdump/dump.dot
#    sed -i "s/href=\"/href=\"request:\/\//g" $HOME/.retdump/dump.svg
    
#    rm $HOME/.retdump/dump.dot
    
    echo ""
#    echo "WEBKIT@@LOAD@@_graph@@file://$HOME/.retdump/dump.svg
#SET@_graph.search_text(\"/ (fcn) sym.main\", False, True, True)
#SET@_searchgraph.set_text(\"/ (fcn) sym.main\")"
#    TXTSEARCHGRAPH="/ (fcn) sym.main"
#    
    echo "TERM@@SEND@@q\ny\nCURRENTBIN=\"$bin\"\ncd \"`dirname "$bin"`\"\nradare2 \"\$CURRENTBIN\" -c \"=h\" -q &\n\n\n
WEBKIT@@LOAD@@_webui@@http://localhost:9090/enyo/"

#    if [[ `cat $HOME/.retdump/asm` == '' ]]; then
#        echo -n '' > $HOME/.retdump/functions
#        $gdb_bin "$bin" -ex "info functions" --batch | cut -d' ' -f3 | cut -d'@' -f1 | while read dis; do
#            if [[ $dis == "" ]]; then continue; fi
#            if [[ $dis == "functions:" ]]; then continue; fi
#            $gdb_bin "$bin" -ex "disassemble $dis" --batch >> $HOME/.retdump/asm

#        done
   
#    nm "$bin" | while read dis; do
#        if [[ "`echo $dis| cut -d' ' -f2`" == "T" ]]; then
#            if [[ "`echo $dis| cut -d' ' -f3`" != "" ]]; then
#                $gdb_bin "$bin" -ex "disassemble `echo $dis| cut -d' ' -f3`" --batch >> $HOME/.retdump/asm
#                echo $dis| cut -d' ' -f3 >> $HOME/.retdump/functions
#                radare2 "$bin" -c "pd@`echo $dis| cut -d' ' -f3` >> $HOME/.retdump/asm3" -q
#            fi
#        fi
#    done
#    fi


echo "TERM@@SEND@@q\ny\nCURRENTBIN=\"$bin\"\ncd \"`dirname "$bin"`\"\n$gdb_bin \"\$CURRENTBIN\"\nset pagination off\nset non-stop on\n
SET@_spinner.stop()
SET@_spinner.set_visible(False)"

#echo "ITER@@start_asm2c"

}

function _origin() {
        echo "TEXT@@LOAD@@_sourceview@@$HOME/.retdump/asm.origin
TEXT@@FIND@@_sourceview@@function main:@@scroll,select"

    echo "TEXT@@LOAD@@_sourceview3@@$HOME/.retdump/asm3.origin
TEXT@@FIND@@_sourceview3@@main:@@scroll,select"

    echo "TEXT@@LOAD@@_sourceview2@@$HOME/.retdump/asm2.origin
TEXT@@FIND@@_sourceview2@@<main>:@@scroll,select"
}

function _homegdb() {
        echo "TEXT@@LOAD@@_sourceview@@$HOME/.retdump/asm.origin
TEXT@@FIND@@_sourceview@@function main:@@scroll,select"
}

function _homeobj() {
    echo "TEXT@@LOAD@@_sourceview2@@$HOME/.retdump/asm2.origin
TEXT@@FIND@@_sourceview2@@<main>:@@scroll,select"
}

function _homeradare2() {
    echo "TEXT@@LOAD@@_sourceview3@@$HOME/.retdump/asm3.origin
TEXT@@FIND@@_sourceview3@@main:@@scroll,select"
}

function _addfunctions() {
    echo "TREE@@END@@_tgdbfunctions@@$FNENTRYCURRENT|$FAENTRYCURRENT"
}

function _deletefunctions() {
    sleep 0.5
}

FAENTRYCURRENT=""
function _entryaddfunctions() {
    FAENTRYCURRENT=$(cut -d '@' -f1 <<< $1)
    echo "SET@entrybreak=\"$FAENTRYCURRENT\""
}

FNENTRYCURRENT=""
function _entrynamefunctions() {
    FNENTRYCURRENT=$(cut -d '@' -f1 <<< $1)
    echo "SET@entrybreak=\"$FNENTRYCURRENT\""
}
