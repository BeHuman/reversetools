function _runltrace() {
    echo "TERM@@SEND@@quit\ny\nltrace \"\$CURRENTBIN\" &\nLTRACEPID=\$!\n"
    echo 'SET@_notebook1.set_current_page(0)'
}

function _stopltrace() {
    echo "TERM@@SEND@@\nkill \$LTRACEPID\ngdb \"\$CURRENTBIN\"\n"
    echo 'SET@_notebook1.set_current_page(0)'
}

function _runstrace() {
    echo "TERM@@SEND@@quit\ny\nstrace \"\$CURRENTBIN\" &\nSTRACEPID=\$!\n"
    echo 'SET@_notebook1.set_current_page(0)'
}

function _stopstrace() {
    echo "TERM@@SEND@@\nkill \$STRACEPID\ngdb \"\$CURRENTBIN\"\n"
    echo 'SET@_notebook1.set_current_page(0)'
}
