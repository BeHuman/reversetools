echo "WEBKIT@@REQUEST@@_graph@@request://"
echo "SET@_graph.set_full_content_zoom(True)"

graphPID=""
function _generate() {
    kill -9 $graphPID
    echo "SET@_spinnergraph.start()
SET@_spinnergraph.set_visible(True)"
    generate "$CURRENTADDR" &
    graphPID=$!
}

function generate() {
    
    echo "$1" > $HOME/.retdump/radare2.cmd
    echo "af" >> $HOME/.retdump/radare2.cmd
    echo "ag > $HOME/.retdump/dump.dot" >> $HOME/.retdump/radare2.cmd
    
    rm -rf $HOME/.retdump/*.svg
    rm -rf $HOME/.retdump/*.dot
    echo "0x00000000" > $HOME/.retdump/radare2_link.cmd
    echo "af" >> $HOME/.retdump/radare2_link.cmd
    echo "ag > $HOME/.retdump/0x00000000.dot" >> $HOME/.retdump/radare2_link.cmd
    
    radare2 "$bin" -i $HOME/.retdump/radare2.cmd -q
    dot -Tsvg -Ln20 -LC2 -o$HOME/.retdump/dump.svg $HOME/.retdump/dump.dot
    sed -i "s/href=\"/href=\"request:\/\//g" $HOME/.retdump/dump.svg
    
    rm $HOME/.retdump/dump.dot
    
    echo ""
    echo "WEBKIT@@LOAD@@_graph@@file://$HOME/.retdump/dump.svg"
    TXTSEARCHGRAPH="/ (fcn) sym.main"
    echo "SET@_spinnergraph.stop()
SET@_spinnergraph.set_visible(False)"
}

function _graph() {
    echo "SET@_spinnergraph.start()
SET@_spinnergraph.set_visible(True)"
    request=$(cut -d '@' -f2 <<< $1)
    sed -i "s/0x00000000/$request/g" $HOME/.retdump/radare2_link.cmd
    radare2 "$bin" -i $HOME/.retdump/radare2_link.cmd -q
    dot -Tsvg -Ln20 -LC2 -o$HOME/.retdump/$request.svg $HOME/.retdump/$request.dot
    sed -i "s/$request/0x00000000/g" $HOME/.retdump/radare2_link.cmd
    sed -i "s/href=\"/href=\"request:\/\//g" $HOME/.retdump/$request.svg
    rm -f $HOME/.retdump/$request.dot
    echo ""
    echo "WEBKIT@@LOAD@@_graph@@file://$HOME/.retdump/$request.svg"
    echo "SET@_spinnergraph.stop()
SET@_spinnergraph.set_visible(False)"
}

function _zoomM() {
    echo "SET@_graph.zoom_out()"
}

function _zoomA() {
    echo "SET@_graph.set_zoom_level(0.5)"
}

function _zoom1() {
    echo "SET@_graph.set_zoom_level(1.0)"
}

function _zoomP() {
    echo "SET@_graph.zoom_in()"
}
