function _run() {
    if [[ $TEMPORUN == 'True' ]]; then
        if [[ $TEMPORISATION -gt 0 ]]; then
            sleep 0.5
            echo "TERM@@SEND@@set logging file $HOME/.retdump/gdb.txt\nset logging on\nrun\n"
            sleep $TEMPORISATION
            echo "TREE@@CLEAR@@_tbreakpoints"
            cat $HOME/.retdump/gdb.txt | while read breaky; do
                cc=`echo "$breaky" | grep -e '^Breakpoint [0-9]*, 0x'`
                if [[ $? -eq 0 ]]; then
                    bb=$(echo $breaky | cut -d' ' -f3)
                    echo "TEXT@@FIND@@_sourceview@@$bb@@scroll,select"
                    echo "TEXT@@FIND@@_sourceview2@@$bb@@scroll,select"
                fi
            done
            sleep 0.5
            echo "TERM@@SEND@@set logging off\n"
            if [[ -f $HOME/.retdump/gdb.txt ]]; then
                rm $HOME/.retdump/gdb.txt
            fi

        else
            echo "TERM@@SEND@@run\n"
        fi
    else
        echo "TERM@@SEND@@run\n"
    fi
    echo 'SET@_notebook1.set_current_page(0)'
}

function _continue() {
    if [[ $TEMPOCONTINUE == 'True' ]]; then
        if [[ $TEMPORISATION -gt 0 ]]; then
            sleep 0.5
            echo "TERM@@SEND@@set logging file $HOME/.retdump/gdb.txt\nset logging on\ncontinue\n"
            sleep $TEMPORISATION
            echo "TREE@@CLEAR@@_tbreakpoints"
            cat $HOME/.retdump/gdb.txt | while read breaky; do
                cc=`echo "$breaky" | grep -e '^Breakpoint [0-9]*, 0x'`
                if [[ $? -eq 0 ]]; then
                    bb=$(echo $breaky | cut -d' ' -f3)
                    echo "TEXT@@FIND@@_sourceview@@$bb@@scroll,select"
                    echo "TEXT@@FIND@@_sourceview2@@$bb@@scroll,select"
                fi
            done
            sleep 0.5
            echo "TERM@@SEND@@set logging off\n"
            if [[ -f $HOME/.retdump/gdb.txt ]]; then
                rm $HOME/.retdump/gdb.txt
            fi
        else
            echo "TERM@@SEND@@continue\n"
        fi
    else
        echo "TERM@@SEND@@continue\n"
    fi
    echo 'SET@_notebook1.set_current_page(0)'
}

function _step() {
    echo "TERM@@SEND@@step\n"
    echo 'SET@_notebook1.set_current_page(0)'
}

function _interrupt() {
    echo "TERM@@SEND@@interrupt\n"
}

function _stop() {
    echo "TERM@@SEND@@stop\n"
}

function _quit() {
    echo "TERM@@SEND@@quit\n"
}

function _break() {
    echo "TERM@@SEND@@break *"
    echo 'SET@_notebook1.set_current_page(0)'
}
function _info_break() {
#    echo "TERM@@SEND@@info breakpoints\n"
#    echo 'SET@_notebook1.set_current_page(0)'
    sleep 0.5
    echo "TERM@@SEND@@set logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo breakpoints\n"
    sleep 0.5
    echo "TREE@@CLEAR@@_tbreakpoints"
    cat $HOME/.retdump/gdb.txt | while read breaky; do
        bb=`echo "$breaky" | sed 's/[<>]//g'`
        cc=`echo "$bb" | grep -e '^[0-9].*$'`
        if [[ $? -eq 0 ]]; then
            echo "TREE@@END@@_tbreakpoints@@$bb"
        fi
    done
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
    echo 'SET@_notebook1.set_current_page(4)'
}

function _disable_break() {
    echo "TERM@@SEND@@disable breakpoints "
    echo 'SET@_notebook1.set_current_page(0)'
}

function _enable_break() {
    echo "TERM@@SEND@@enable breakpoints "
    echo 'SET@_notebook1.set_current_page(0)'
}

function _delete_break() {
    echo "TERM@@SEND@@delete breakpoints "
    echo 'SET@_notebook1.set_current_page(0)'
}

function _info_register() {
    echo "TERM@@SEND@@info register $"
    echo 'SET@_notebook1.set_current_page(0)'
}

function _x2s() {
    echo "TERM@@SEND@@x/s "
    echo 'SET@_notebook1.set_current_page(0)'
}

function _x2x() {
    echo "TERM@@SEND@@x/2x $"
    echo 'SET@_notebook1.set_current_page(0)'
}

function _registerall() {
    sleep 0.5
#    $gdb_bin "$bin" -ex "info registers all" --batch > dump.registers
    
    echo "TERM@@SEND@@set logging file $HOME/.retdump/gdb.txt\nset logging on\ninfo registers all\n"
    sleep 0.5
    echo "TEXT@@LOAD@@_viewregisters@@$HOME/.retdump/gdb.txt"
    sleep 0.5
    echo "TERM@@SEND@@set logging off\n"
    if [[ -f $HOME/.retdump/gdb.txt ]]; then
        rm $HOME/.retdump/gdb.txt
    fi
    echo 'SET@_notebook1.set_current_page(4)'
}

asm2cPID=""
CURRENTADDR=""
function _tgdbfunctions() {
    kill -9 $asm2cPID
    echo ""
    arg1="$(cut -d '@' -f1 --complement <<< $@ )"
    arg2="`echo $arg1 | cut -d'|' -f1`"
    arg1="`echo $arg1 | cut -d'|' -f2 | sed 's/0x//'`"
    $gdb_bin "$bin" -ex "disassemble 0x$arg1" --batch > $HOME/.retdump/asm.$arg1
    radare2 "$bin" -c "pd@0x$arg1 >> $HOME/.retdump/asm3.$arg1" -q
    objdump -Mintel -d "$bin" | awk -v RS= '/^'$arg1'.*<.*>/'  > $HOME/.retdump/asm2

    #objdump -S "$bin" | awk '/^[[:xdigit:]]+ <${arg2}>:$/{flag=1;next}/^[[:xdigit:]]+ <.*>:$/{flag=0}flag'  > $HOME/.retdump/asm2.$arg2
    
    echo "TEXT@@LOAD@@_sourceview@@$HOME/.retdump/asm.$arg1
TEXT@@LOAD@@_sourceview2@@$HOME/.retdump/asm2
TEXT@@LOAD@@_sourceview3@@$HOME/.retdump/asm3.$arg1
TEXT@@FIND@@_sourceview@@function $arg2:@@scroll,select
TEXT@@FIND@@_sourceview2@@<$arg2>:@@scroll,select
TEXT@@FIND@@_sourceview3@@sym.$arg2:@@scroll,select"
    CURRENTADDR="0x$arg1"
#    echo "ITER@@start_asm2c"
#    echo "ITER@@generate"
}
