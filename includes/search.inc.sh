TXTSEARCHTEXT=""
function _search() {
    TXTSEARCHTEXT=$(cut -d '@' -f1 <<< "$@")
    echo "TEXT@@FIND@@_sourceview@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview2@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview3@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview4@@$TXTSEARCHTEXT@@scroll,select"
}

function _searchtextavant() {
  
    echo "TEXT@@FIND@@_sourceview@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview2@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview3@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview4@@$TXTSEARCHTEXT@@scroll,select"
}

function _searchtextsuivant() {
    echo "TEXT@@FIND@@_sourceview@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview2@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview3@@$TXTSEARCHTEXT@@scroll,select"
    echo "TEXT@@FIND@@_sourceview4@@$TXTSEARCHTEXT@@scroll,select"
}

TXTSEARCHGRAPH=""
function _searchgraph() {
    TXTSEARCHGRAPH=$(cut -d '@' -f1 <<< "$@")
    echo "SET@_graph.search_text(\"$TXTSEARCHGRAPH\", False, True, True)"
    sleep 0.1
    echo "SET@_graph.mark_text_matches(\"$TXTSEARCHGRAPH\", True, 0)"
}

function _searchgraphsuivant() {
    echo "SET@_graph.search_text(\"$TXTSEARCHGRAPH\", False, True, True)"
    sleep 0.1
    echo "SET@_graph.mark_text_matches(\"$TXTSEARCHGRAPH\", True, 0)"
}

TXTSEARCHWEBUI=""
function _searchwebui() {
    TXTSEARCHWEBUI=$(cut -d '@' -f1 <<< "$@")
    echo "SET@_webui.search_text(\"$TXTSEARCHWEBUI\", False, True, True)"
    sleep 0.1
    echo "SET@_webui.mark_text_matches(\"$TXTSEARCHWEBUI\", True, 0)"
}

function _searchwebuisuivant() {
    echo "SET@_webui.search_text(\"$TXTSEARCHWEBUI\", False, True, True)"
    sleep 0.1
    echo "SET@_webui.mark_text_matches(\"$TXTSEARCHWEBUI\", True, 0)"
}
