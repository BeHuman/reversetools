#!/bin/bash
pseudocodePID=""
function _generatepseudocode() {
    kill -9 $pseudocodePID
    echo "SET@_spinnerpseudocode.start()
SET@_spinnerpseudocode.set_visible(True)"
    asm2c "$HOME/.retdump/asm2" > $HOME/.retdump/asm2.c
    echo "TEXT@@LOAD@@_sourceview4@@$HOME/.retdump/asm2.c
TEXT@@FIND@@_sourceview4@@<main>:@@scroll,select"

    echo "SET@_spinnerpseudocode.stop()
SET@_spinnerpseudocode.set_visible(False)"
   pseudocodePID=$!
}

function start_asm2c() {
    asm2c "$HOME/.retdump/asm2" > $HOME/.retdump/asm2.c
    echo "TEXT@@LOAD@@_sourceview4@@$HOME/.retdump/asm2.c
TEXT@@FIND@@_sourceview4@@<main>:@@scroll,select"
}
function asm2c() {
    
    ja=""
    je=""
    jb=""
    jae=""
    jbe=""
    jnz=""
    addr=""
    tab=""
    fn="0"
    cat "$1"| while read line; do
        if [[ $line == "" ]]; then
            continue
        fi
        addr="`echo $line | cut -d$':' -f1`:    "
        #addr=""
        
        out=`echo "$line" | grep '[0-9]*\ <.*>:'`
        if [[ $? -eq 0 ]]; then
            if [[ "$fn" == "1" ]]; then
                echo "void function $line() {"
                tab="    "
            else
                echo "void function $line() {"
                fn="1"
                tab="    "
            fi
            continue
        fi
        
        out=`echo "$line" | grep 'mov '`
        if [[ $? -eq 0 ]]; then
            line1=`echo $line | sed -e 's/.*mov//' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            v1=`echo -n "$line1" | cut -d',' -f1`
            v2=`echo -n "$line1" | cut -d',' -f2`
            echo "${tab}${addr}$v1 = $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep -e 'mov.'`
        if [[ $? -eq 0 ]]; then
            line1=`echo $line | sed -e 's/.*mov.//' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            v1=`echo -n "$line1" | cut -d',' -f1`
            v2=`echo -n "$line1" | cut -d',' -f2`
            echo "${tab}${addr}$v1 = $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'lea'`
        if [[ $? -eq 0 ]]; then
            line1=`echo $line | sed -e 's/.*lea//' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            v1=`echo -n "$line1" | cut -d',' -f1`
            v2=`echo -n "$line1" | cut -d',' -f2`
            echo "${tab}${addr}$v1 = *$v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'cmp '`
        if [[ $? -eq 0 ]]; then
            line1=`echo $line | sed -e 's/.*cmp//g'  -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            v1=`echo -n "$line1" | cut -d',' -f1`
            v2=`echo -n "$line1" | cut -d',' -f2`
            jb="if ( $v1 < $v2 )"
            ja="if ( $v1 > $v2 )"
            je="if ( $v1 == $v2 )"
            jbe="if ( $v1 <= $v2 )"
            jae="if ( $v1 >= $v2 )"
            jnz="if ( $v1 != $v2 )"
            continue
        fi
        
        out=`echo "$line" | grep 'test '`
        if [[ $? -eq 0 ]]; then
            line1=`echo $line | sed -e 's/.*test//g'  -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            v1=`echo -n "$line1" | cut -d',' -f1`
            jb="if ( $v1 < 0 )"
            ja="if ( $v1 > 0 )"
            je="if ( $v1 == 0 )"
            jbe="if ( $v1 <= 0 )"
            jae="if ( $v1 >= 0 )"
            jnz="if ( $v1 != 0 )"
            continue
        fi
        
        #################################################JUMP###########################################
        out=`echo "$line" | grep 'jb '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jb//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jb goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jc '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jc//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jb goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jl '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jl//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jb goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'ja '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*ja//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$ja goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jg '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jg//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$ja goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'je '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*je//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$je goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jz '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jz//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$je goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jnz '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jnz//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jnz goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        out=`echo "$line" | grep 'jne '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jne//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jnz goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jae '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jae//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jae goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jge '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jge//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jae goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jnb '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jnb//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jae goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jnc '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jnc//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jae goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jbe '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jbe//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jbe goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jle '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jle//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$jbe goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'jmp'`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*jmp//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}goto 0x0$v1;" | sed 's/[%$]//g'
            continue
        fi
        
        #################################################JUMP###########################################
        
        out=`echo "$line" | grep 'add '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*add//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*add//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 = $v1 + $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'sub '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*sub//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*sub//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 = $v1 - $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'neg '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*neg//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$v1 = -$v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'imul '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*imul//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*imul//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            v3=`echo $line | sed -e 's/.*imul//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f3`
            if [[ "$v3" != "" ]]; then
                echo "${tab}${addr}$v1 = $v2*$v3;" | sed 's/[%$]//g'
            else
                echo "${tab}${addr}$v1 = $v1*$v2;" | sed 's/[%$]//g'
            fi
            continue
        fi
        
        out=`echo "$line" | grep 'mul '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*imul//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*imul//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            v3=`echo $line | sed -e 's/.*imul//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f3`
            if [[ "$v3" != "" ]]; then
                echo "${tab}${addr}$v1 = $v2*$v3;" | sed 's/[%$]//g'
            else
                echo "${tab}${addr}$v1 = $v1*$v2;" | sed 's/[%$]//g'
            fi
            continue
        fi
        
        out=`echo "$line" | grep 'idiv '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*idev//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            echo "${tab}${addr}rax = rdx:rax / $v1;" | sed 's/[%$]//g'
            echo "${tab}       rdx = rdx:rax % $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'and '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*and//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*and//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 &= $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'not '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*not//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$v1 =~ $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'or '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*or//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*or//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 |= $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'shl '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*shl//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*shl//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 <<= $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'sal '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*sal//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*sal//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 <<= $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'shr '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*shr//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*shr//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 >>= $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'sar '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*sar//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*sar//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 >>= $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'xor '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*xor//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f1`
            v2=`echo $line | sed -e 's/.*xor//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' | cut -d',' -f2`
            echo "${tab}${addr}$v1 ^= $v2;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'inc '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*inc//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$v1++;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'dec '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*dec//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}$v1--;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'ret '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*ret//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo "${tab}${addr}return $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        out=`echo "$line" | grep 'rep '`
        if [[ $? -eq 0 ]]; then
            v1=`echo $line | sed -e 's/.*rep//g' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//'`
            echo -n "${tab}${addr}for ( int i=0; i<ecx; i++ ) goto $v1;" | sed 's/[%$]//g'
            continue
        fi
        
        echo "${tab}/*$line*/"
    done
    echo "}"
     
}
