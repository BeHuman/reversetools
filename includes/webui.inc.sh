#echo "WEBKIT@@REQUEST@@_webui@@request://"
echo "SET@_webui.set_full_content_zoom(True)"

#function _webui() {
#    request=$(cut -d '@' -f2 <<< $1)
#    sed -i "s/0x00000000/$request/g" $HOME/.retdump/radare2_link.cmd
#    radare2 "$bin" -i $HOME/.retdump/radare2_link.cmd -q
#    dot -Tsvg -Ln20 -LC2 -o$HOME/.retdump/$request.svg $HOME/.retdump/$request.dot
#    sed -i "s/$request/0x00000000/g" $HOME/.retdump/radare2_link.cmd
#    sed -i "s/href=\"/href=\"request:\/\//g" $HOME/.retdump/$request.svg
#    rm -f $HOME/.retdump/$request.dot
#    echo ""
#    echo "WEBKIT@@LOAD@@_graph@@file://$HOME/.retdump/$request.svg"
#}


function _zoomM1() {
    echo "SET@_webui.zoom_out()"
}

function _zoomA1() {
    echo "SET@_webui.set_zoom_level(0.5)"
}

function _zoom2() {
    echo "SET@_webui.set_zoom_level(1.0)"
}

function _zoomP1() {
    echo "SET@_webui.zoom_in()"
}
